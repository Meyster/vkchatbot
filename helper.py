import vk_api
from vk_api.longpoll import VkEventType, VkLongPoll
import random
import json

token = "cd9b406a92ec88229e1cc7ee74fabf972d3a461bac8d94d2b6c088d42edff9080708c8b29c9c660ae010e"
vk = vk_api.VkApi(token=token)
vk.get_api()
longpoll = VkLongPoll(vk)

def get_button(label, color, payload = ""):
    return {
        "action": {
            "type": "text",
            "payload": json.dumps(payload),
            "label": label
        },
        "color": color
    }

keyboard = {
    "one_time": True,
    "buttons": [
    [get_button(label="Расписание", color="positive"),
     get_button(label="TODO лист", color="positive")],
    [get_button(label="Мероприятия", color="positive"),
     get_button(label="Мини-игры", color="negative")],
    ]
}
keyboard = json.dumps(keyboard, ensure_ascii = False).encode('utf-8')
keyboard = str(keyboard.decode('utf-8'))

keyboard2 = {
    "one_time": True,
    "buttons": [
    [get_button(label="Создать новое расписание", color="positive")],        
    [get_button(label="Редактировать текущее расписание", color="positive")],
    [get_button(label="Удалить текущее расписание", color="positive")],
    [get_button(label="Назад", color="negative")],
    ]
}
keyboard2 = json.dumps(keyboard2, ensure_ascii = False).encode('utf-8')
keyboard2 = str(keyboard2.decode('utf-8'))

keyboard3 = {
    "one_time": True,
    "buttons": [
    [get_button(label="Создать лист", color="positive")], 		 #--------------------#
    [get_button(label="Редактировать лист", color="positive")],	 #					  #	
    [get_button(label="Просмотреть лист", color="positive")],	 #создание функционала#
    [get_button(label="Удалить лист", color="positive")],		 #	для todo листа    #
    [get_button(label="Назад", color="negative")],				 #--------------------#
    ]
}
keyboard3 = json.dumps(keyboard3, ensure_ascii = False).encode('utf-8')
keyboard3 = str(keyboard3.decode('utf-8'))

keyboard4 = {
    "one_time": True,
    "buttons": [
    [get_button(label="Добавить новое мероприятие", color="positive")],
    [get_button(label="Удалить существующее мероприятие", color="positive")],
    [get_button(label="Просмотр запланированных мероприятий", color="positive")],
    [get_button(label="Просмотр ближайших мероприятий", color="positive")],
    [get_button(label="Назад", color="negative")],
    ]
}
keyboard4 = json.dumps(keyboard4, ensure_ascii = False).encode('utf-8')
keyboard4 = str(keyboard4.decode('utf-8'))

while True:
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW and (event.from_chat or event.from_user):
            response = event.text.lower()
            if response == "начать":
                vk.method('messages.send', {'user_id': event.user_id, 'message': 'Привет, выбери что-нибудь!',
                                            'random_id': random.randint(-2147483648, +2147483648), 'keyboard': keyboard})
            if (response == "расписание"):
                vk.method('messages.send', {'user_id': event.user_id, 'message': 'Good luck, bro!',
                                            'random_id': random.randint(-2147483648, +2147483648), 'keyboard': keyboard2})
            if (response == "todo лист"):
                vk.method('messages.send', {'user_id': event.user_id, 'message': 'Good luck, bro!',
                                            'random_id': random.randint(-2147483648, +2147483648), 'keyboard': keyboard3})
            if (response == "мероприятия"):
                vk.method('messages.send', {'user_id': event.user_id, 'message': 'Good luck, bro!',
                                            'random_id': random.randint(-2147483648, +2147483648), 'keyboard': keyboard4})
            if (response == "назад"):
                vk.method('messages.send', {'user_id': event.user_id, 'message': 'Good luck, bro!',
                                            'random_id': random.randint(-2147483648, +2147483648), 'keyboard': keyboard})
