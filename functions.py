import random
from vk_api.keyboard import VkKeyboard, VkKeyboardColor

def sendMessage(vk_session, id_type, id, message = None, keyboard = None):
    vk_session.method('messages.send', {id_type: id, 'message': message, 'random_id': random.randint(-2147483648, 2147483648), 'keyboard': keyboard})

def createKeyboard(response):
    keyboard = VkKeyboard(one_time = False)
    if response == "начать" or response == "назад":
        keyboard.add_button('РАСПИСАНИЕ', color = VkKeyboardColor.POSITIVE)
        keyboard.add_button('TODO ЛИСТ', color = VkKeyboardColor.POSITIVE)
        keyboard.add_line()
        keyboard.add_button('МЕРОПРИЯТИЯ', color = VkKeyboardColor.POSITIVE)
        keyboard.add_button('МИНИ ИГРЫ', color = VkKeyboardColor.POSITIVE)

    elif response == "расписание" or response == "создать новое расписание" or response == "редактировать текущее расписание" or response == "удалить текущее расписание" or response == "просмотреть расписание":
        keyboard.add_button('СОЗДАТЬ НОВОЕ РАСПИСАНИЕ', color = VkKeyboardColor.POSITIVE)
        keyboard.add_line()
        keyboard.add_button('РЕДАКТИРОВАТЬ ТЕКУЩЕЕ РАСПИСАНИЕ', color = VkKeyboardColor.POSITIVE)
        keyboard.add_line()
        keyboard.add_button('УДАЛИТЬ ТЕКУЩЕЕ РАСПИСАНИЕ', color = VkKeyboardColor.POSITIVE)
        keyboard.add_line()
        keyboard.add_button('ПРОСМОТРЕТЬ РАСПИСАНИЕ', color = VkKeyboardColor.POSITIVE)
        keyboard.add_line()
        keyboard.add_button('НАЗАД', color = VkKeyboardColor.NEGATIVE)

    elif response == "мероприятия" or response == "добавить новое мероприятие" or response == "удалить мероприятие" or response == "просмотреть мероприятия":
        keyboard.add_button('ДОБАВИТЬ НОВОЕ МЕРОПРИЯТИЕ', color = VkKeyboardColor.POSITIVE)
        keyboard.add_line()
        keyboard.add_button('УДАЛИТЬ МЕРОПРИЯТИЕ', color = VkKeyboardColor.POSITIVE)
        keyboard.add_line()
        keyboard.add_button('ПРОСМОТРЕТЬ МЕРОПРИЯТИЯ', color = VkKeyboardColor.POSITIVE)
        keyboard.add_line()
        keyboard.add_button('НАЗАД', color = VkKeyboardColor.NEGATIVE)

    elif response == "todo лист" or response == "добавить задачу" or response == "удалить задачу" or response == "просмотреть список задач":
        keyboard.add_button('ДОБАВИТЬ ЗАДАЧУ', color = VkKeyboardColor.POSITIVE)
        keyboard.add_line()
        keyboard.add_button('УДАЛИТЬ ЗАДАЧУ', color = VkKeyboardColor.POSITIVE)
        keyboard.add_line()
        keyboard.add_button('ПРОСМОТРЕТЬ СПИСОК ЗАДАЧ', color = VkKeyboardColor.POSITIVE)
        keyboard.add_line()
        keyboard.add_button('НАЗАД', color=VkKeyboardColor.NEGATIVE)

    keyboard = keyboard.get_keyboard()
    return keyboard