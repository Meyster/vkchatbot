from database.connection import connect

def createList(u_id, date, name, priority):
    connection = connect()
    with connection:
        cur = connection.cursor()
        cur.execute(f'USE timetabledb')
        cur.execute(f'INSERT INTO todotable(u_id, date, name, priority) VALUES("{u_id}", "{date}", "{name}", "{priority}")')
        connection.commit()
    connection.close()

def showList():
    ret = ""
    connection = connect()
    with connection:
        cur = connection.cursor()
        cur.execute(f'USE timetabledb')
        cur.execute(f'SELECT * FROM todotable ORDER BY priority')
        results = cur.fetchall()
        for row in results:
            date = row["date"]
            name = row["name"]
            num = row["num"]
            priority = row["priority"]
            if priority == 1:
                ret += ("№" + str(num) + ' ' + name + " до " + str(date) + " | ВЫСОКИЙ ПРИОРИТЕТ!" + "\n")
            else:
                ret += ("№" + str(num) + ' ' + name + " до " + str(date) + "\n")
        return ret

def delElemToDo(num):
    connection = connect()
    with connection:
        cur = connection.cursor()
        cur.execute(f'USE timetabledb')
        cur.execute(f'DELETE from todotable WHERE num = %s', (num))
    connection.close()