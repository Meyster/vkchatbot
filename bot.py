import vk_api
from vk_api.longpoll import VkEventType, VkLongPoll
from functions import sendMessage, createKeyboard
from timetable import creatett, showtt, deltt, modifytt, delNumTt
from calendarlist import createCalendar, showCalendar, delElemOfCalendar
from todolist import createList, showList , delElemToDo

vk_session = vk_api.VkApi(token = "cd9b406a92ec88229e1cc7ee74fabf972d3a461bac8d94d2b6c088d42edff9080708c8b29c9c660ae010e")
vk_session.get_api()
longpoll = VkLongPoll(vk_session)

back = "назад"
hello = "начать"
timetable = "расписание"
calendar = "мероприятия"
tDList = "todo лист"

unknownCommand = "Простите, но вы используете неизвестную мне команду!"
helloAns = "Привет, выбери нужную опцию!"
timetableAns = "Теперь необходимо выбрать то, что вы хотите сделать!"
error = "Ошибка!"

commandCrateTt = "создать новое расписание"
commandModTt = "редактировать текущее расписание"
commandDelTt = "удалить текущее расписание"
commandShowTt = "просмотреть расписание"
commandCreateCal = "добавить новое мероприятие"
commandDelCal = "удалить мероприятие"
commandShowCal = "просмотреть мероприятия"
commandCreateTodo = "добавить задачу"
commandDelTodo = "удалить задачу"
commandShowTodo = "просмотреть список задач"

infoCreateTt = "Для создания расписания вам необходимо использовать команду:\n!ctt [Номер р-я] [День] [Предмет] [Время начала]"
infoModTt = "Для редактирования расписания вам необходимо использовать команду:\n1. Для изменения конкретного поля:\n!mtt [mod] [Номер р-я] [День] [Время начала] [Предмет / Время] [Новое значение]\n2. Для удаления конкретного поля:\n!mtt [del] [Номер р-я] [День] [Время начала]"
infoDelTt = "Для удаления всего конкретного расписания вам необходимо использовать:\n!dtt [Номер расписания]"
infoShowTt = "Для просмотра расписания вам необходимо использовать команду:\n!stt [Номер расписания]"
infoCreateCal = "Для создания мероприятия вам необходимо использовать команду:\n!ccal [Дата мероприятия] [Время мероприятия] [Название мероприятия]"
infoDelCal = "Для удаления мероприятия вам необходимо использовать команду:\n!dcal [Номер мероприятия]"
infoShowCal = "Для просмотра мероприятий вам необходимо использовать команду:\n!scal [Начальная дата] [Конечная дата]"
infoDelTodo = "Для удаления задачи вам необходимо использовать команду:\n!dtodo [Номер задачи]"
infoCreateTodo = "Для добавления задачи вам необходимо использовать команду:\n!ctodo [Дата] [Название задачи] [Приоритет]"
infoShowTodo = "Для вывода списка задач вам необходимо использовать команду:\n!stodo"

createTt = "!ctt"
showTt = "!stt"
modTt = "!mtt"
deleteTt = "!dtt"

createCal = "!ccal"
showCal = "!scal"
deleteCal = "!dcal"

createToDo = "!ctodo"
showToDo = "!stodo"
deletetodo = "!dtodo"

while True:
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW:
            highResponse = event.text
            response = event.text.lower()
            keyboard = createKeyboard(response)
            command = response.split()[0]
            word = event.text
            if event.from_user:
                if response == hello:
                    sendMessage(vk_session, 'user_id', event.user_id, message = helloAns, keyboard = keyboard)
                elif response == timetable:
                    sendMessage(vk_session, 'user_id', event.user_id, message = timetableAns, keyboard = keyboard)
                elif response == back:
                    sendMessage(vk_session, 'user_id', event.user_id, message = helloAns, keyboard = keyboard)
                elif response == calendar:
                    sendMessage(vk_session, 'user_id', event.user_id, message=timetableAns, keyboard = keyboard)
                elif response == tDList:
                    sendMessage(vk_session, 'user_id', event.user_id, message=timetableAns, keyboard = keyboard)

                elif response == commandCrateTt:
                    sendMessage(vk_session, 'user_id', event.user_id, message = infoCreateTt, keyboard = keyboard)
                elif response == commandModTt:
                    sendMessage(vk_session, 'user_id', event.user_id, message=infoModTt, keyboard = keyboard)
                elif response == commandDelTt:
                    sendMessage(vk_session, 'user_id', event.user_id, message=infoDelTt, keyboard = keyboard)
                elif response == commandShowTt:
                    sendMessage(vk_session, 'user_id', event.user_id, message=infoShowTt, keyboard = keyboard)
                elif response == commandCreateCal:
                    sendMessage(vk_session, 'user_id', event.user_id, message=infoCreateCal, keyboard = keyboard)
                elif response == commandDelCal:
                    sendMessage(vk_session, 'user_id', event.user_id, message=infoDelCal, keyboard = keyboard)
                elif response == commandShowCal:
                    sendMessage(vk_session, 'user_id', event.user_id, message=infoShowCal, keyboard = keyboard)
                elif response == commandCreateTodo:
                    sendMessage(vk_session, 'user_id', event.user_id, message=infoCreateTodo, keyboard = keyboard)
                elif response == commandDelTodo:
                    sendMessage(vk_session, 'user_id', event.user_id, message=infoDelTodo, keyboard = keyboard)
                elif response == commandShowTodo:
                    sendMessage(vk_session, 'user_id', event.user_id, message=infoShowTodo, keyboard = keyboard)

                elif command == createTt:
                    uid = event.user_id
                    num = word.split()[1]
                    day = word.split()[2]
                    lesson = word.split()[3]
                    timestart = word.split()[4]
                    creatett(uid, day, lesson, timestart, num)
                elif command == showTt:
                    num = word.split()[1]
                    count = 0
                    message = ""
                    names = ["Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"]
                    while (count < 6):
                        name = names[count]
                        per = showtt(num, names[count])
                        message += (name + ":\n" + per + "\n")
                        count += 1
                    sendMessage(vk_session, 'user_id', event.user_id, message = message)
                elif command == modTt:
                    option = word.split()[1]
                    num = word.split()[2]
                    day = word.split()[3]
                    timestart = word.split()[4]
                    if option == "del":
                        deltt(num, day, timestart)
                    elif option == "mod":
                        if word.split()[5] == "Предмет":
                            field = "Предмет"
                            text = word.split()[6]
                            modifytt(num, day, timestart, field, text)
                        elif word.split()[5] == "Время":
                            field = "Время"
                            text = word.split()[6]
                            modifytt(num, day, timestart, field, text)
                        else:
                            print("")
                    else:
                        print("")
                elif command == deleteTt:
                    num = word.split()[1]
                    delNumTt(num)
                elif command == createCal:
                    uid = event.user_id
                    date = word.split()[1]
                    time = word.split()[2]
                    name = word.split()[3]
                    createCalendar(uid, time, name, date)
                elif command == showCal:
                    date1 = word.split()[1]
                    date2 = word.split()[2]
                    mes = showCalendar(date1, date2)
                    if mes == 228:
                        sendMessage(vk_session, 'user_id', event.user_id, message=error)
                    else:
                        sendMessage(vk_session, 'user_id', event.user_id, message=mes)
                elif command == deleteCal:
                    num = word.split()[1]
                    delElemOfCalendar(num)
                elif command == createToDo:
                    uid = event.user_id
                    date = word.split()[1]
                    name = word.split()[2]
                    priority = word.split()[3]
                    createList(uid, date, name, priority)
                elif command == showToDo:
                    mes = showList()
                    sendMessage(vk_session, 'user_id', event.user_id, message=mes)
                elif command == deletetodo:
                    num = word.split()[1]
                    delElemToDo(num)