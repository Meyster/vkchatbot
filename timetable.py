from database.connection import connect

def creatett(u_id, day, lesson, time_start, num):
    connection = connect()
    with connection:
        cur = connection.cursor()
        cur.execute(f'USE timetabledb')
        cur.execute(f'INSERT INTO timetable(u_id, day, lesson, time_start, num) VALUES("{u_id}", "{day}", "{lesson}", "{time_start}", "{num}")')
        connection.commit()
    connection.close()

def showtt(num, day):
    ret = ""
    connection = connect()
    with connection:
        cur = connection.cursor()
        cur.execute(f'USE timetabledb')
        cur.execute(f'SELECT * FROM timetable WHERE num = %s AND day = %s ORDER BY time_start', (num, day))
        results = cur.fetchall()
        for row in results:
            lesson = row["lesson"]
            timestart = row["time_start"]
            ret += (str(timestart) + " - " + lesson + "\n")
        return ret

def deltt(num, day, timestart):
    connection = connect()
    with connection:
        cur = connection.cursor()
        cur.execute(f'USE timetabledb')
        cur.execute(f'DELETE FROM timetable WHERE num = %s AND day = %s AND time_start = %s', (num, day, timestart))
    connection.close()

def modifytt(num, day, timestart, field, perem):
    connection = connect()
    with connection:
        cur = connection.cursor()
        cur.execute(f'USE timetabledb')
        if field == "Предмет":
            cur.execute(f'UPDATE timetable SET lesson = %s WHERE num = %s AND day = %s AND time_start = %s', (perem, num, day, timestart))
        elif field == "Время":
            cur.execute(f'UPDATE timetable SET time_start = %s WHERE num = %s AND day = %s AND time_start = %s', (perem, num, day, timestart))
        else:
            connection.close()
    connection.close()

def delNumTt(num):
    connection = connect()
    with connection:
        cur = connection.cursor()
        cur.execute(f'USE timetabledb')
        cur.execute(f'DELETE from timetable WHERE num = %s', (num))
    connection.close()